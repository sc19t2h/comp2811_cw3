﻿//
// Created by twak on 11/11/2019.
//
#include<math.h>
#include "the_player.h"

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    //TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
    //buttons -> at( updateCount++ % buttons->size() ) -> init( i );
    qint64 t=this->duration();
    int ss = 1000;
    int mi = ss * 60;
    int hh = mi * 60;
    int dd = hh * 24;
    long day = t / dd;
    long hour = (t - day * dd) / hh;
    long minute = (t - day * dd - hour * hh) / mi;
    long second = (t - day * dd - hour * hh - minute * mi) / ss;
    QString hou = QString::number(hour,10);
    QString min = QString::number(minute,10);
    QString sec = QString::number(second,10);
    timelabe->setText(hou + ":" + min + ":" + sec);
    progress->setMaximum(t/1000);
    progress->setValue(this->position()/1000);
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}

void ThePlayer::playpause(){
    if(this->state()==QMediaPlayer::State::PlayingState){
        playbtn->setText("play");
        pause();
    }else{
        playbtn->setText("pause");
        play();
    }
}

void ThePlayer::setWidget(QPushButton* playbtn,QLabel* timelabe,QSlider *progress){
    this->playbtn=playbtn;
    this->timelabe=timelabe;
    this->progress=progress;
}
void ThePlayer::changePos(int pos){
    int t=this->position()/1000;
    if(abs(t-pos)>=1)
        setPosition(pos*1000);
}
void ThePlayer::repeatplay()
{
    stop();
    setPosition(0);
    play();
    playbtn->setText("pause");
}
void ThePlayer::moveLeft(){
    int i;
    for(i=0;i<infos->size();i++)
        if(&infos -> at(i)==buttons->at(0)->info)
            break;
    if(i>0){
        int j=0;
        for(i=i-1;j<buttons->size();i++,j++)
            buttons -> at(j)->init(& infos -> at(i));
    }
}

void ThePlayer::moveRight(){
    int i;
    for(i=0;i<infos->size();i++)
        if(&infos ->at(i)==buttons->at(buttons->size()-1)->info)
            break;
    if(infos->size()>i+1){
        int j=0;
        for(i=i-buttons->size()+2;j<buttons->size();i++,j++)
            buttons -> at(j)->init(& infos -> at(i));
    }
}

void ThePlayer::setRate(int index){
    qreal rate=1;
    switch(index){
    case 1:
        rate=0.5;
        break;
    case 2:
        rate=0.75;
        break;
    case 4:
        rate=1.5;
        break;
    case 5:
        rate=2.0;
        break;
    default:
        rate=1.0;
        break;
    }
    setPlaybackRate(rate);
}
